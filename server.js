// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel
const { createServer } = require("http");
const { parse } = require("url");
const next = require("next");
const LRUCache = require("lru-cache");

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

// This is where we cache our rendered HTML pages
const ssrCache = new LRUCache({
  max: 100,
  maxAge: 1000 * 60 * 60 // 1 Hour
});

// app.prepare().then(() => {
//   createServer((req, res) => {
//     // Be sure to pass `true` as the second argument to `url.parse`.
//     // This tells it to parse the query portion of the URL.
//     const parsedUrl = parse(req.url, true);
//     const { pathname, query } = parsedUrl;
//
//     if (pathname === "/") {
//       app.render(req, res, "/index", query);
//     } else if (pathname === "/a") {
//       app.render(req, res, "/b", query);
//     } else if (pathname === "/b") {
//       app.render(req, res, "/a", query);
//     } else if (pathname.indexOf("entity:node") !== -1) {
//       app.render(req, res, "/blank", query);
//     } else {
//       /* 404 not found */
//       handle(req, res, parsedUrl);
//     }
//   }).listen(3000, err => {
//     if (err) throw err;
//     console.log("> Ready on http://localhost:3000");
//   });
// });

const express = require("express");

app
  .prepare()
  .then(() => {
    const server = express();

    // Use the `renderAndCache` utility defined below to serve pages
    server.get("/", (req, res) => {
      renderAndCache(req, res, "/");
    });

    server.get("/", (req, res) => {
      renderAndCache(req, res, "/");
    });

    server.get("/entity:node/:id", (req, res) => {
      // app.render(req, res, "/blank", { id: req.params.id });
      renderAndCache(req, res, "/blank", { id: req.params.id });
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(port, err => {
      if (err) throw err;
      console.log("> Ready on http://localhost:3000");
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });

/*
   * NB: make sure to modify this to take into account anything that should trigger
   * an immediate page change (e.g a locale stored in req.session)
   */
function getCacheKey(req) {
  return `${req.url}`;
}

async function renderAndCache(req, res, pagePath, queryParams) {
  const key = getCacheKey(req);

  // If we have a page in the cache, let's serve it
  if (ssrCache.has(key)) {
    res.setHeader("x-cache", "HIT");
    res.send(ssrCache.get(key));
    return;
  }

  try {
    // If not let's render the page into HTML
    const html = await app.renderToHTML(req, res, pagePath, queryParams);

    // Something is wrong with the request, let's skip the cache
    if (res.statusCode !== 200) {
      res.send(html);
      return;
    }

    // Let's cache this page
    ssrCache.set(key, html);

    res.setHeader("x-cache", "MISS");
    res.send(html);
  } catch (err) {
    app.renderError(err, req, res, pagePath, queryParams);
  }
}

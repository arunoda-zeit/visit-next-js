import React from "react";
import Link from "next/link";
import styled from "styled-components";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const mainMenu = [];

    mainMenu.push(
      <li key="home-menu">
        <Link href="/">
          <a className={this.props.router.asPath === "/" ? "active" : null}>Home</a>
        </Link>
      </li>
    );

    if (this.props.menusList) {
      this.props.menusList.map(item => {
        if (item.attributes.menu_name === "main") {
          const path = `/${item.attributes.link.uri}`;

          mainMenu.push(
            <li key={item.attributes.uuid}>
              <Link href={path}>
                <a className={path === this.props.router.asPath ? "active" : null}>{item.attributes.title}</a>
              </Link>
            </li>
          );
        }
      });
    }

    return (
      <Wrap>
        <ul>{mainMenu}</ul>
      </Wrap>
    );
  }
}

const Wrap = styled.div`
  border: 1px solid #ccc;
  margin: 10px 0 20px 0;
  padding: 10px;

  ul {
    display: table;
    list-style: none;
    margin: 0 auto;
    padding: 0;

    li {
      display: inline-block;

      &:first-child {
        a {
          margin-left: 0;
        }
      }

      a {
        display: inline-block;
        color: #409a00;
        margin-left: 15px;

        &:hover {
          text-decoration: none;
        }
        &.active {
          color: #c4161c;
          cursor: default;
          text-decoration: none;
        }
      }
    }
  }
`;

import React from "react";
import Link from "next/link";
import styled from "styled-components";

const Logo = () => (
  <Link href="/">
    <Home>
      <img src="/static/logo.png" alt="" />
    </Home>
  </Link>
);

export default Logo;

const Home = styled.a`
  display: table;
  margin: 0 auto;
  cursor: pointer;
`;

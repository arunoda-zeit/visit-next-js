import App, { Container } from "next/app";
import React from "react";
import axios from "axios";
import styled from "styled-components";

import api from "../utilities/api";
import Logo from "../src/components/header/Logo";
import Menu from "../src/components/header/Menu";

export default class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    const menus = await axios.get(api.drupal.get.all_menus);
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return {
      menuData: menus.data.data,
      currentRoute: router,
      pageProps: pageProps
    };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Logo />
        <Menu menusList={this.props.menuData} router={this.props.currentRoute} />
        <Wrap>
          <Component {...pageProps} />
        </Wrap>
      </Container>
    );
  }
}

const Wrap = styled.div`
  display: table;
  max-width: 500px;
  margin: 0 auto;
`;

import Head from "next/head";

export default class extends React.Component {
  render() {
    return (
      <div className="index_wrap">
        <Head>
          <title>VisitDenmark Home</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        Hello Denmark! This is our Home page.
      </div>
    );
  }
}
